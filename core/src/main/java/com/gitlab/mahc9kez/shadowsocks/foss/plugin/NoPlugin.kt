package com.github.shadowsocks.plugin

import com.gitlab.mahc9kez.shadowsocks.foss.Core.app

object NoPlugin : Plugin() {
    override val id: String get() = ""
    override val label: CharSequence get() = app.getText(com.gitlab.mahc9kez.shadowsocks.foss.core.R.string.plugin_disabled)
}
