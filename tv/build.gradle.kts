plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
}

setupApp()

android.defaultConfig.applicationId = "com.gitlab.mahc9kez.shadowsocks.foss.tv"

dependencies {
    implementation("androidx.leanback:leanback-preference:1.1.0-alpha05")
}
