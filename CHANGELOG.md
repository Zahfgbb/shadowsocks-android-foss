### 5.1.4 - 2020-11-02
- Android 5.x is no longer supported for now
- Minor bug fixes

### 5.1.2 - 2020-08-11
- Replace shadowsocks-libev with shadowsocks-rust
- Add support for plain method
- Hardware acceleration for AES
- TCP fast open and hosts are deprecated for now
- Local DNS also running over TCP
- Support for Android 11
- (mobile) Add machine learning technology powered built-in QR code scanner

### 5.0.6 - 2020-04-06
- Minor bug fixes

### 5.0.5 - 2020-02-25
- Fix a bug in policy routing
- Plugin library has been updated to 1.3.4
- Minor bug fixes

### 5.0.1 - 2020-01-20
- Add subscription support
- New icon and theme
- Remove built-in ACL rules except Bypass LAN

### 4.8.7a - 2019-12-25
- Rename to Shadowsocks FOSS
- Remove proprietary dependencies (GMS, Fabric and Firebase Crashlytics)
- Update Gradle, Android Gradle plugin and dependencies
- Change default config to route=bypass-lan, remoteDns=1dot1dot1dot1.cloudflare-dns.com, udpdns=true, ipv6=true
- Delete zh-rCN (Simplified Chinese) translation
- Change minSdkVersion from 21 to 23
- Change gradle config from APK Splits to Product Flavors, add version name suffix
- change mipmap format from WebP to PNG
- Publish to F-Droid

### 4.8.7 - 2019-12-20
- First release of Shadowsocks FOSS
